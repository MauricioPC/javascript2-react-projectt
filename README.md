# React project

_This application is developed with react and is an application to order a series of products, such as uber, rappi, or as an online store, the steps to follow within the application are:_
    
    
* When entering the page you will see a list of products.
* For each product you can see its description with just one click.
* If you want to get this product you can add it to the cart.
* Once you have added your products to the cart, you will be able to order.



## Status project 🚀

_The objective was fulfilled since the application performs what was expected, as something extra it was possible to make a connection to a FireBase database, a navigation bar, also work was done on the design part, the necessary tests were carried out and it is completely responsive_


### Pre requirements 📋

```
It is necessary to run npm install before starting to run this project
```

### Install 🔧


```
npx create-react-app unit-testing
npm install --save-dev jest

```


## Running the tests ⚙️

_Some unit tests were done where some individual functions were tested. Some integration tests were also used, we focused on the combination of different unit blocks. For example, when we want to test the functionality of a React component that depends on another component_

### Analyze the evidence end-to-end 🔩

_Here we are looking to test entire application workflows, that is, entire scenarios like logging in to a certain page. Basically we try to reproduce the same thing that a real user would do with the application, in a certain way it is like doing manual tests but of course, e2e tests are automated, for example:_

```
it("should add meals to cart", async () => {
		const button = await screen.findByRole("button");
		expect(button).toBeInTheDocument();

		const input = container.querySelector('input[type="number"]');
		expect(input).toBeInTheDocument();

		userEvent.click(button);

		expect(dispatch).not.toHaveBeenCalled();

		input.value = "10";

		userEvent.click(button);

		expect(dispatch).toHaveBeenCalled();
	});
```

## Deployment 📦

_Below are the tools of react itself that were used to be able to comply with the development of the application_

* _Components_ - A block of reusable code, a piece of UI with content, styles and behavior defined: it contains all the HTML, CSS and JS necessary to function, we use it to create models.
* _State_ - are the internal variables of the component
* _Hooks_ - These allow us to use the state of some react features without the need to use a class..
* _Portals_ - They provide a first-class option to render children into a DOM node that exists outside of the parent component's DOM hierarchy. The first argument (child) is any React-renderable child, such as an element, string, or fragment.
* _Reducers_ - This helped us with the management of some states, since it contains more capabilities and we use it for the actions of: add, update and remove from the cart.
* _styles_ - In the part of the styles we decided to take the option to import.

## Build with 🛠️

* [Visual Studio Code](https://code.visualstudio.com/download) - is a code editor redefined and optimized for building and debugging modern web and cloud applications.
* [Node](https://nodejs.org/es/download/) - It is a runtime environment for JavaScript built on V8, Chrome's JavaScript engine.
* [React](https://es.reactjs.org/) - A JavaScript library for building user interfaces.

## versioning 📌

We used [gitlab](http://semver.org/) for the version. helps you automate the builds, integration, and verification of your code. With SAST, DAST, code quality analysis, plus pipelines that enable.

## Authors ✒️

_The participants of this project are:_

* **Juan Antonio Mendoza Sanchez** - *Tests*
* **Antonio Nicolas Herrera Boites** - *Designer* 
* **Mario Eduardo Salas Ornelas** - *Documentation*
* **Fernando Vargas Rivera** - *Database*
* **Maurio Perez Campos** - *Version control*
* **Fabian Alejandro Landeros** - *Validations*

## Expresiones de Gratitud 🎁

* Thanks to the training of the Bedu experts for the teachings 📢.
* Thanks to the participants of this project since they worked as a good team🍺.
